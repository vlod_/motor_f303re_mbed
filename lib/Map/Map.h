#ifndef MAP_H_
#define MAP_H_

class Map{
  public:
  float inmin,inmax,outmin,outmax;
  Map(float inmin_,float inmax_,float outmin_,float outmax_){
    inmin = inmin_;
    inmax = inmax_;
    outmin = outmin_;
    outmax = outmax_;
  }
  float calc(float inval){
    return ((inval-inmin)*(outmax-outmin)/(inmax-inmin) + outmin);
  }
};

#endif