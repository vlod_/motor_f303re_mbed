#include "StepBipolar.h"

uint8_t StepBipolar::stepbinary[4]={0b1010,0b1001,0b0101,0b0110};

StepBipolar::StepBipolar(PinName p1, PinName p2, PinName p3, PinName p4):bo(p1,p2,p3,p4){
  startperiod = 5000;
  minperiod = 1500;
  decreperiod = 100;
  init();
}

void StepBipolar::init(){
  timeout.detach();
  brake=DISABLE;
  phase=0;
  bo=0b0000;
  countsum=0;
  count=0;
  period = startperiod;
  negative = false;
  state = IDLE;
}

void StepBipolar::step(int targetstep){
  if(targetstep==0){
    timeout.detach();
    bo=0b0000;
    state = IDLE;
    return;
  }
  else if(targetstep < 0){
    negative = true;
    target = -targetstep;
  }
  else{
    negative = false;
    target = targetstep;
  }
  count = 0;
  period = startperiod;
  state = STEP;
  timeout.attach_us(callback(this,&StepBipolar::cb),10);
}

void StepBipolar::speed(int period_){
  if(period_==0){
    timeout.detach();
    bo=0b0000;
    state = IDLE;
    return;
  }
  else if(period_ < 0){
    negative = true;
    minperiod = -period_;
  }
  else{
    negative = false;
    minperiod = period_;
  }
  if(minperiod > startperiod)
    period = minperiod;
  else
    period = startperiod;
  count = 0;
  target = 0;
  state = SPEED;
  timeout.attach_us(callback(this,&StepBipolar::cb),10);
}

void StepBipolar::cb(){
  if(period > minperiod){
    period-=decreperiod;
    if(period < minperiod)
      period = minperiod;
  }
  if((count < target) || (state==SPEED)){
    timeout.attach_us(callback(this,&StepBipolar::cb),period);
    if(negative == true){
      if(phase == 0)
        phase = 3;
      else
        phase--;
      countsum--;
    }
    else{
      if(phase == 3)
        phase = 0;
      else
        phase++;
      countsum++;
    }
    count++;
    bo=stepbinary[phase];
  }
  else{
    timeout.detach();
    state = DONE;
    if(brake==DISABLE){
      bo=0b0000;
    }
  }
}