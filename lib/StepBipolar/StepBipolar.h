#ifndef STEPBIPOLAR_H_
#define STEPBIPOLAR_H_
#include <mbed.h>

enum SBState{
  IDLE=0,
  STEP=1,
  DONE=2,
  SPEED=3,
  BRAKE=4,
};

class StepBipolar{
  private:
  static uint8_t stepbinary[4];
  SBState state;
  char phase;
  bool brake;
  bool negative;
  int countsum;
  int count;
  int target;
  uint16_t startperiod;
  uint16_t minperiod;
  uint16_t period;
  uint16_t decreperiod;
  BusOut bo;
  Timeout timeout;
  public:
  StepBipolar(PinName,PinName,PinName,PinName);
  void init();
  void step(int);
  void speed(int);
  void cb();
  inline void setstartperiod(uint16_t arg){
    startperiod = arg;
  }
  inline void setminperiod(uint16_t arg){
    minperiod = arg;
  }
  inline void setdecreperiod(uint16_t arg){
    decreperiod = arg;
  }
  inline void setbrake(bool arg){
    brake = arg;
  }
  inline int getcount(){
    return count;
  }
  inline int getcountsum(){
    return countsum;
  }
  inline SBState getstate(){
    return state;
  }
  inline int getperiod(){
    return period;
  }
};

#endif