#include "StepBipolarPwm.h"

//uint8_t StepBipolarPwm::stepbinary[4]={0b1010,0b1001,0b0101,0b0110};
float StepBipolarPwm::steparray[16][4]={
  {1.0,0  ,0  ,0}  ,{0.75,0   ,0.25,0}   ,{0.5,0  ,0.5,0}  ,{0.25,0   ,0.75,0},
  {0  ,0  ,1.0,0}  ,{0   ,0.25,0.75,0}   ,{0  ,0.5,0.5,0}  ,{0   ,0.75,0.25,0},
  {0  ,1.0,0  ,0}  ,{0   ,0.75,0   ,0.25},{0  ,0.5,0  ,0.5},{0   ,0.25,0   ,0.75},
  {0  ,0  ,0  ,1.0},{0.25,0   ,0   ,0.75},{0.5,0  ,0  ,0.5},{0.75,0   ,0   ,0.25}
};

StepBipolarPwm::StepBipolarPwm(PinName p1_, PinName p2_, PinName p3_, PinName p4_):p1(p1_),p2(p2_),p3(p3_),p4(p4_){
  startperiod = 5000;
  minperiod = 1500;
  decreperiod = 100;
  init();
}

void StepBipolarPwm::init(){
  timeout.detach();
  brake=DISABLE;
  phase=0;
  //bo=0b0000;
  countsum=0;
  count=0;
  period = startperiod;
  negative = false;
  state = IDLE;
}

void StepBipolarPwm::step(int targetstep){
  if(targetstep==0){
    timeout.detach();
    //bo=0b0000;
    state = IDLE;
    return;
  }
  else if(targetstep < 0){
    negative = true;
    target = -targetstep;
  }
  else{
    negative = false;
    target = targetstep;
  }
  count = 0;
  period = startperiod;
  state = STEP;
  timeout.attach_us(callback(this,&StepBipolarPwm::cb),10);
}

void StepBipolarPwm::speed(int period_){
  if(period_==0){
    timeout.detach();
    //bo=0b0000;
    state = IDLE;
    return;
  }
  else if(period_ < 0){
    negative = true;
    minperiod = -period_;
  }
  else{
    negative = false;
    minperiod = period_;
  }
  if(minperiod > startperiod)
    period = minperiod;
  else
    period = startperiod;
  count = 0;
  target = 0;
  state = SPEED;
  timeout.attach_us(callback(this,&StepBipolarPwm::cb),10);
}

void StepBipolarPwm::cb(){
  if(period > minperiod){
    period-=decreperiod;
    if(period < minperiod)
      period = minperiod;
  }
  if((count < target) || (state==SPEED)){
    timeout.attach_us(callback(this,&StepBipolarPwm::cb),period);
    if(negative == true){
      if(phase == 0)
        phase = 15;
      else
        phase--;
      countsum--;
    }
    else{
      if(phase == 15)
        phase = 0;
      else
        phase++;
      countsum++;
    }
    count++;
    p1=steparray[phase][0];
    p2=steparray[phase][1];
    p3=steparray[phase][2];
    p4=steparray[phase][3];
  }
  else{
    timeout.detach();
    state = DONE;
    if(brake==DISABLE){
      //bo=0b0000;
    }
  }
}