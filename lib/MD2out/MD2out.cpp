#include "MD2out.h"
#include <mbed.h>

MD2Out::MD2Out(PinName pwm1,PinName pwm2):p1(pwm1),p2(pwm2){
  p1.period_us(PWM_PERIOD_US);
  p2.period_us(PWM_PERIOD_US);
  p1.write(0);
  p2.write(0);
  inverted=0;
}

void MD2Out::setDuty(float duty){
  if(duty == 0){
    p1.write(0);
    p2.write(0);
  }
  else if(inverted==0){
    if(duty > 0){
      p1.write(0);
      p2.write(duty);
    }
    else{
      p1.write(-duty);
      p2.write(0);
    }
  }
  else{
    if(duty > 0){
      p1.write(duty);
      p2.write(0);
    }
    else{
      p1.write(0);
      p2.write(-duty);
    }
  }
}

void MD2Out::brake(){
  p1.write(1);
  p2.write(1);
}

void MD2Out::invert(int rev){
  if(rev==0)
    inverted=1;
  else
    inverted=0;
}