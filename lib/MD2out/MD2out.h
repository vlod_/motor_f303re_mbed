#ifndef MD2OUT_H_
#define MD2OUT_H_
#include <mbed.h>
#define PWM_PERIOD_US 40

class MD2Out{
  private:
  PwmOut p1;
  PwmOut p2;
  char inverted;

  public:
  MD2Out(PinName,PinName);
  void setDuty(float);
  void brake();
  void invert(int);
};

#endif