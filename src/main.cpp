#include <mbed.h>
#include "MD2out.h"
#include "simplePID.h"
//#include "StepBipolar.h"
#include "StepBipolarPwm.h"
#include "Map.h"

/*
ポテンショメータ .read()
90度 0.567
45度 0.416
 0度 0.298
*/

//DEBUG
Serial pc(USBTX,USBRX,9600);

//PID
float kp=0.5,ki=0,kd=0;
PID pid1(kp,ki,kd);

//ポテンショメーター
AnalogIn ad1(PC_2); //右
AnalogIn ad2(PC_3); //左

//DCモーター制御
//MD2Out md1(PA_13,PA_14);
//MD2Out md2(PA_15,PB_7);
//MD2Out md3(PA_1,PA_4);
//MD2Out md4(PB_0,PC_1);//動作不良

//ステッピングモーター制御
StepBipolarPwm sb1(PA_13,PA_14,PA_15,PB_7);

Ticker ticker;
char tickflag = 0;
char pidflag = 0;

void latchflag(){
  tickflag = 1;
}

Map map(0,1.0,5000,1500); //0から1の範囲を15000から1500の範囲に変換
//(PID出力最小,PID出力最大,ステッピング速度最小,ステッピング速度最大)

void pid(){
  float input = ad1.read();
  pid1.setproc(input); //ポテンショの値をを入力
  float output = pid1.calc(); //PIDの計算
  bool neg=false;
  if(output < 0){    //一度絶対値にして値を変換
    neg=true;
    output = -output;
  }
  output = map.calc(output);
  float dif = input-pid1.gettarget(); //目標値と入力の差の絶対値
  if(dif < 0)
    dif=-dif;
  if(output > 4900 && dif < 0.007){  //出力が小さく、差が小さければPID終了
    output = 0;
    pidflag = 0;
  }
  if(neg) //絶対値を戻す
    output = -output;
  sb1.speed(output); //ステッピングモーターに反映(ステップ周期)
}

int main() {
  //ticker.attach_us(latchflag,10*1000); //10ms
  sb1.setminperiod(1500); //step()メソッド時の最小周期(最大回転速度)
  sb1.setstartperiod(2000); //step()メソッド時の初期周期(徐々に加速する)
  sb1.setdecreperiod(20); //step()メソッド時のステップごとの加速量
  pid1.setinlim(1,0); //PID入力制限(最大,最小)
  pid1.setoutlim(1,-1); //PID出力制限(最大,最小)
  pid1.settarget(0.57); //PID目標入力
  printf("Hello world\n");
  while(1) {
    if(tickflag == 1){
      static int ct = 0;
      ct++;
      tickflag = 0;
      if(pidflag)
        pid(); //PID実行
      if(ct%100==0) //10*50=500ms
        printf("ad1=%f | out=%f | period=%d\n",ad1.read(),pid1.getoutput(),sb1.getperiod());
    }
    if(pc.readable()){ //キーボード入力(Serial)
      char c = pc.getc();
      switch(c){
        case '1':
          pid1.settarget(0.567);
          pidflag = 1;
          break;
        case '2':
          pid1.settarget(0.416);
          pidflag = 1;
          break;
        case '3':
          pid1.settarget(0.298);
          pidflag = 1;
          break;
        case '7':
          sb1.step(300*3);
          break;
        case '8':
          sb1.step(-300*3);
          break;
      }
    }
  }
}